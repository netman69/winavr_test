/*
 * Implementation of blake2s hashing algorithm, based on the code found here:
 *   https://tools.ietf.org/html/draft-saarinen-blake2-01
 *
 * Copyright (c) 2015 IETF Trust and the persons identified as authors of the
 * code. All rights reserved.
 * Redistribution and use in source and binary
 * forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * Redistributions of source code must retain the
 * above copyright notice, this list of conditions and the following
 * disclaimer.
 * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither
 * the name of Internet Society, IETF or IETF Trust, nor the names of specific
 * contributors, may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <avr/pgmspace.h>
#include <string.h> /* memcpy() */

#include "blake2s32.h"

#define OUTLEN 32

/* Context */
uint8_t ctx_b[64]; /* Input buffer */
uint32_t ctx_h[8]; /* Chained state */
uint32_t ctx_tl; /* Total number of bytes (low dword) */
uint32_t ctx_th; /* High dword of total */
uint8_t ctx_c; /* pointer for b[] */

#ifndef ROTR32
#define ROTR32(x, y) (((x) >> (y)) ^ ((x) << (32 - (y))))
#endif

static const uint32_t initv[8] PROGMEM = {
	0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
	0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
};

static const uint8_t sigma[160] PROGMEM = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
	14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3,
	11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4,
	7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8,
	9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13,
	2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9,
	12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11,
	13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10,
	6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5,
	10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0
};

static const uint8_t map[] PROGMEM = {
	0, 4,  8, 12,
	1, 5,  9, 13,
	2, 6, 10, 14,
	3, 7, 11, 15,
	0, 5, 10, 15,
	1, 6, 11, 12,
	2, 7,  8, 13,
	3, 4,  9, 14
};

void compress(void) {
	uint8_t i, a, b, c, d;
	uint32_t v[16];

	memcpy(v, ctx_h, 8 * 4);
	memcpy_P(v + 8, initv, 8 * 4);

	v[12] ^= ctx_tl;
	v[13] ^= ctx_th;
	if (ctx_c != 0) /* Last block flag set ? */
		v[14] = ~v[14];

	/* Original version converts little endian ctx_b to big endian before this loop */
	i = 0;
	while (i < 160) { /* Ten rounds */
		d = ((i & 14) << 1); /* d is temporary storage before setting it */
		a = pgm_read_byte(&(map[d++]));
		b = pgm_read_byte(&(map[d++]));
		c = pgm_read_byte(&(map[d++]));
		d = pgm_read_byte(&(map[d]));
		v[a] = v[a] + v[b] + ((uint32_t *) ctx_b)[pgm_read_byte(&(sigma[i++]))];
		v[d] = ROTR32(v[d] ^ v[a], 16);
		v[c] = v[c] + v[d];
		v[b] = ROTR32(v[b] ^ v[c], 12);
		v[a] = v[a] + v[b] + ((uint32_t *) ctx_b)[pgm_read_byte(&(sigma[i++]))];
		v[d] = ROTR32(v[d] ^ v[a], 8);
		v[c] = v[c] + v[d];
		v[b] = ROTR32(v[b] ^ v[c], 7);
	}

	for(i = 0; i < 8; ++i)
		ctx_h[i] ^= v[i] ^ v[i + 8];
}

/* Initialize the state. key is optional */
void blake2s_init(const void *key, uint8_t keylen) {
	uint8_t i;

	memcpy_P(ctx_h, initv, 8 * 4); /* State, "param block" */
	ctx_h[0] ^= 0x01010000 ^ (keylen << 8) ^ OUTLEN;

	ctx_tl = 0; /* Input count low dword */
	ctx_th = 0; /* High dword */
	ctx_c = 0; /* Pointer within buffer */

	if (keylen > 0) {
		for (i = keylen; i < 64; ++i) /* Zero pad key */
			ctx_b[i] = 0;
		blake2s_update(key, keylen);
		ctx_c = 64; /* At the end */
	}
}

/* Update with new data */
void blake2s_update(const void *in, size_t inlen) {
	size_t i;

	for (i = 0; i < inlen; ++i) {
		if (ctx_c == 64) { /* Buffer full ? */
			ctx_tl += 64; /* Add counters */
			if (ctx_tl < 64)
				++ctx_th;
			ctx_c = 0; /* Counter to zero, must go before compress, 0 signals not last block */
			compress(); /* Compress (not last) */
		}
		ctx_b[ctx_c++] = ((const uint8_t *) in)[i];
	}
}

/* Finalize */
void blake2s_final(void *out) {
	ctx_tl += ctx_c; /* Mark last block offset */
	if (ctx_tl < ctx_c)
		++ctx_th;

	while (ctx_c < 64) /* Fill up with zeros */
		ctx_b[ctx_c++] = 0;
	compress(); /* Final block detected because ctx_c != 0 */

	memcpy(out, ctx_h, OUTLEN); /* On little endian system, need conversion here */
}

void blake2s(void *out, const void *key, uint8_t keylen, const void *in, size_t inlen) {
	blake2s_init(key, keylen);
	blake2s_update(in, inlen);
	blake2s_final(out);
}
