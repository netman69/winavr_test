#ifndef __UART_H__
#define __UART_H__

#include <stdio.h>

extern FILE uart;

extern void uart_init(void);
extern void uart_putchar(char c);
extern char uart_getchar(void);

#define BAUD 57600

#endif /* __UART_H__ */
