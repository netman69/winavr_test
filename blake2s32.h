#ifndef __BLAKE2S32_H__
#define __BLAKE2S32_H__

#include <stdint.h>
#include <stdlib.h>

/* Output data has fixed size set in blake2s32.c */
/* Keylen should be between 0 and 32, 0 means no key */

extern void blake2s_init(const void *key, uint8_t keylen);
extern void blake2s_update(const void *in, size_t inlen);
extern void blake2s_final(void *out);
extern void blake2s(void *out, const void *key, uint8_t keylen, const void *in, size_t inlen);

#endif /* __BLAKE2S32_H__ */
