#include "uart.h"
#include "blake2s32.h"

#include <avr/io.h>
#include <util/delay.h>

int main(void) {
	uart_init();
	DDRB |= (1<<5); // Set LED as an output
	PORTB |= (1<<5); // Turn it on.

	while(1) {
		int i;
		fprintf(&uart, "Hello, World!\n");
		
		unsigned char buf[32];
		for (i = 0; i < 1000; ++i)
			blake2s(&buf, "", 0, "message", 7);

		for (i = 0; i < 32; ++i) {
			int c = (int) (unsigned char) buf[i];
			fprintf(&uart, "%02x", c);
		}
		fprintf(&uart, "\n");
		
		PORTB ^= (1<<5);
		_delay_ms(500);
	}
}
